import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * Imports que deber�an de hacer para la funcionalidad.
 * 
import deloitte.academy.lesson01.entity.Habitacion;
import deloitte.academy.lesson01.entity.Huesped;
*/

/**
 * Clase operations, incluye todas las operaciones permitidas para el hotel.
 * 
 * @author Javier Adalid
 *
 */
public class HotelOperations {
	private static final Logger LOGGER = Logger.getLogger(Operations.class.getName());

	/**
	 * Permite hacer check in en el hotel.
	 * 
	 * @param numHab:  numero de habitacion, type int.
	 * @param huesped: Huesped creado, type Huesped.
	 */
	public static void checkIn(int numHab, Huesped huesped) {
		boolean encontrada = false;

		for (Habitacion hab : Habitacion.habitaciones) {
			if (hab.getNumeroHabitacion() == numHab && hab.isDisponible()) {
				encontrada = true;
				hab.setDisponible(false);
				hab.setHuesped(huesped);
				LOGGER.log(Level.INFO, "Check In correcto! \n");
				break;
			}
		}
		if (!encontrada)
			LOGGER.log(Level.SEVERE, "Error! No es posible realizar el check in. \n");

	}

	/**
	 * M�todo que permite hacer check out en el hotel
	 * 
	 * @param numHab:  n�mero de habitacion, type int
	 * @param importe: total a cobrar (sin descuento) type double.
	 */
	public static void checkOut(int numHab, double importe) {
		boolean encontrada = false;

		for (Habitacion hab : Habitacion.habitaciones) {
			if (!hab.isDisponible()) {
				if (hab.getNumeroHabitacion() == numHab) {
					encontrada = true;
					hab.setDisponible(true);
					hab.getHuesped().setTotal(importe);
					LOGGER.log(Level.INFO, "Check Out! El total a pagar es de: $" + hab.getHuesped().cobrar() + "\n");
					break;
				}
			}
		}
		if (!encontrada)
			LOGGER.log(Level.SEVERE, "Error! no es posible realizar el checkout. \n");

	}

	/**
	 * M�todo que despliega todas las habitaciones que se encuentran disponibles
	 * para ser reservadas.
	 */
	public static void verHabitacionesDisponibles() {
		for (Habitacion hab : Habitacion.habitaciones) {
			if (hab.isDisponible())
				System.out.println(hab.getNumeroHabitacion() + " ------------- " + hab.getTipo().toString());
		}
		System.out.println("");
	}
}

