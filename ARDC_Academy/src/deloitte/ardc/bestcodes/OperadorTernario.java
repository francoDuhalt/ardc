package deloitte.ardc.bestcodes;

public class OperadorTernario {

		/**
		 * M�todo con operador ternario que permite determinar cual de los dos n�meros
		 * es el mayor.
		 * 
		 * @param num1: n�mero de tipo entero.
		 * @param num2: n�mero de tipo entero.
		 * @return num1 o num2 dependiendo de cu�l sea el mayor.
		 */
		public int numeroMayor(int num1, int num2) {
			return (num1 > num2) ? num1 : num2;
		}

		/**
		 * M�todo con operador ternario que permite regresar la suma de ambos n�meros si
		 * es que estos dos no son iguales.
		 * 
		 * @param num1: n�mero de tipo entero.
		 * @param num2: n�mero de tipo entero.
		 * @return la suma de num1 y num 2 si la condici�n es verdadera. Cero si es
		 *         falsa.
		 */
		public int sumaDiferentes(int num1, int num2) {
			return (num1 != num2) ? (num1 + num2) : 0;
		}

		/**
		 * M�todo que recibe un n�mero y determina si es mayor o menor a 1000.
		 * 
		 * @param num1: n�mero entero.
		 * @return Regresa un texto indicando si es menor o mayor a mil.
		 */
		public String menorMayorMil(int num1) {
			return (num1 < 1000) ? "Menor a mil" : "Mayor a mil";
		}

		/**
		 * M�todo con if simple. Permite determinar qu� n�mero es el menor entre los 2.
		 * 
		 * @param num1: n�mero entero.
		 * @param num2: n�mero entero.
		 * @return regresa num1 o num2 dependiendo de cu�l sea el n�mero menor.
		 */
		public int numeroMenor(int num1, int num2) {
			int resultado = 0;
			if (num1 < num2) {
				resultado = num1;
			} else {
				resultado = num2;
			}

			return resultado;
		}

		/**
		 * M�todo con elseif que permite determinar si los dos n�meros de entrada son
		 * iguales o cu�l es el menor de los dos.
		 * 
		 * @param num1: n�mero entero.
		 * @param num2: n�mero entero.
		 * @return un string que determina cu�l es el n�mero menor o si son iguales.
		 */
		public String numeroMenorIgual(int num1, int num2) {
			String resultado = "";
			if (num1 < num2) {
				resultado = "El n�mero menor es: " + num1;
			} else if (num1 > num2) {
				resultado = "El n�mero menor es: " + num2;
			} else if (num1 == num2) {
				resultado = "Ambos n�meros son iguales";
			}
			return resultado;
		}

	}

