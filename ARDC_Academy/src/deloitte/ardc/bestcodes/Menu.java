package deloitte.academy.lesson01.entity;

import java.util.Scanner;
import java.util.logging.Logger;

import deloitte.academy.lesson01.machine.Machine;

public class Menu {
	/*
	 * Logger required to display error/alert messages to the user,
	 * java.util.logging.Logger is a must.
	 */
	private static final Logger LOGGER = Logger.getLogger(Menu.class.getName());

	/**
	 * This method displays a menu, so the user can interact with the machine, the
	 * user can ask for display products, buy products, fill the machine, change
	 * costs, show total sells, etc. The menu should be executed while the
	 * {@code var execute} has a true value assigned.
	 * 
	 * The execution of {@code showMenu()} is the following:
	 * 
	 * 1. The creation of a Machine object
	 * 2. The execution of {@code createProductsAndThenFillTheMachineWithThem()} method
	 * 3. The execution of {@code kindlySayHello} method
	 * 4. The execution of the menu itself
	 * 
	 * @exception RECOVER/FATAL it depends on how the program behaves and what type
	 *                          of error occurs during the execution of the program.
	 * 
	 * @see FATAL
	 * @see RECOVER
	 */
	public static void showMenu() {
		Machine machine = new Machine();
		createProductsAndThenFillTheMachineWithThem();
		kindlySayHello();

		var execute = true;

		try {
			// Creating scanner object to get user's option, java.util.Scanner is a must.
			Scanner sc = new Scanner(System.in);
			do {
				displayOptions();
				System.out.print("Type an option (just numbers): ");
				/*
				 * Upper casing the string will allow user to type lower/upper case without
				 * throwing an error
				 */
				String option = sc.nextLine().toUpperCase();

				switch (option) {
				case "1":
					Machine.displayProducts();
					break;
				case "2":
					System.out.print("Please, enter the product code: ");
					/*
					 * Upper casing the string will allow user to type lower/upper case without
					 * throwing an error
					 */
					String code = sc.nextLine().toUpperCase();

					// Every static Machine method should have a try catch to avoid errors
					try {
						Machine.sellProduct(code);
					} catch (Exception e) {
						LOGGER.info(Errors.RECOVER.getDescription());
					}

					break;
				case "3":
					try {
						Machine.showSoldProducts();
					} catch (Exception e) {
						LOGGER.info(Errors.RECOVER.getDescription());
					}

					break;
				case "4":
					try {
						machine.read();
					} catch (Exception e) {
						LOGGER.info(Errors.RECOVER.getDescription());
					}

					break;
				case "5":
					try {
						machine.update();
					} catch (Exception e) {
						LOGGER.info(Errors.RECOVER.getDescription());
					}
					break;
				case "6":
					try {
						machine.delete();
					} catch (Exception e) {
						LOGGER.info(Errors.RECOVER.getDescription());
					}
					break;
				case "7":
					try {
						machine.create();
					} catch (Exception e) {
						LOGGER.info(Errors.RECOVER.getDescription());
					}
					break;
				case "8":
					execute = !execute;
					LOGGER.info("\n\nMY TIME ON EARTH HAS COME :(\n\n");
					break;
				default:
					LOGGER.info("\n\nAN PLEASE TYPE A VALID OPTION\n\n");
				}
			} while (execute);
		} catch (Exception exception) {
			LOGGER.info(Errors.FATAL.getDescription());
		}
	}

	/**
	 * This method sets the initial state to the machine, it begins creating
	 * products, and then filling the machine with them.
	 */
	public static void createProductsAndThenFillTheMachineWithThem() {
		// Creating the products
		Product A1 = new Product("A1", "CHOCOLATE", 10.50, 10);
		Product A2 = new Product("A2", "DORITOS", 15.50, 4);
		Product A3 = new Product("A3", "COCA", 22.50, 2);
		Product A4 = new Product("A4", "GOMITAS", 8.75, 6);
		Product A5 = new Product("A5", "CHIPS", 30.00, 10);
		Product A6 = new Product("A6", "JUGO", 15, 2);
		Product B1 = new Product("B1", "GALLETAS", 10, 3);
		Product B2 = new Product("B2", "CANELITAS", 120.00, 6);
		Product B3 = new Product("B3", "HALLS", 10.10, 10);
		Product B4 = new Product("B4", "TARTA", 3.14, 10);
		Product B5 = new Product("B5", "SABRITAS", 15.55, 0);
		Product B6 = new Product("B6", "CHEETOS", 12.25, 4);
		Product C1 = new Product("C1", "ROCALETA", 10, 1);
		Product C2 = new Product("C2", "RANCHERITOS", 14.75, 6);
		Product C3 = new Product("C3", "RUFLES", 13.15, 10);
		Product C4 = new Product("C4", "PIZZA FRIA", 22, 9);

		// Adding first row
		Machine.addProduct(A1);
		Machine.addProduct(A2);
		Machine.addProduct(A3);
		Machine.addProduct(A4);
		Machine.addProduct(A5);
		Machine.addProduct(A6);

		// Adding second row
		Machine.addProduct(B1);
		Machine.addProduct(B2);
		Machine.addProduct(B3);
		Machine.addProduct(B4);
		Machine.addProduct(B5);
		Machine.addProduct(B6);

		// Adding third row
		Machine.addProduct(C1);
		Machine.addProduct(C2);
		Machine.addProduct(C3);
		Machine.addProduct(C4);
	}

	// This method just says hello to the user in a kind way
	public static void kindlySayHello() {
		System.out.println("...::: WELCOME TO THE FANTASTIC MACHINE :::...");
	}

	/**
	 * This method just displays the menu options with their respective description
	 * and number.
	 */
	public static void displayOptions() {
		System.out.println("\n...::: OPTIONS :::...");
		System.out.println("1. Display products");
		System.out.println("2. Buy product");
		System.out.println("3: Show sold products");
		System.out.println("4: Search product");
		System.out.println("5: Update product");
		System.out.println("6: Delete product");
		System.out.println("7: Create product");
		System.out.println("8. Kill machine\n\n");
	}
}

