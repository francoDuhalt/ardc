package deloitte.ardc.bestcodes;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

	/**
	 * La clase Articulos contiene todos los atributos que cada articulo debe tener.
	 * Se trabajan los constructores, getters and setters para poderlos mandar.
	 * llamar en la Clase Main.
	 * 
	 * @author dareyes@externosdeloittemx.com
	 * @version 1.0
	 * @since 2020-03-10 
	 *
	 */
	public abstract class Articulos {

		// Creacion de lista para guardar los objetos de los articulos creados.
		public static ArrayList<Articulos> productos = new ArrayList<Articulos>();

		// Declaracion de atributos
		private String codigo;
		private String articulo;
		private double precio;
		private int cantidad;
		private int vendidos;


		// Constructores.
		/**
		 * Constructor de la clase Articulos. Permite crear objetos y heredar atributos.
		 */
		public Articulos() {

		}

		/**
		 * El constructor permite agregar atributos a los objetos creados de la clase
		 * Articulos.
		 * 
		 * @param codigo   Debe ser de tipo String.
		 * @param articulo Debe ser de tipo String.
		 * @param precio   Debe ser de tipo Double.
		 * @param cantidad Debe ser de tipo Int.
		 */
		public Articulos(String codigo, String articulo, double precio, int cantidad) {
			super();
			this.codigo = codigo;
			this.articulo = articulo;
			this.precio = precio;
			this.cantidad = cantidad;
			// Agregar todos los registros a la lista.
			productos.add(this);
		}

		// Getters and Setters de los Atributos de la Clase Articulos.

		public String getCodigo() {
			return codigo;
		}

		public void setCodigo(String codigo) {
			this.codigo = codigo;
		}

		public String getArticulo() {
			return articulo;
		}

		public void setArticulo(String articulo) {
			this.articulo = articulo;
		}

		public double getPrecio() {
			return precio;
		}

		public void setPrecio(double precio) {
			this.precio = precio;
		}

		public int getCantidad() {
			return cantidad;
		}

		public void setCantidad(int cantidad) {
			this.cantidad = cantidad;
		}

		public int getVendidos() {
			return vendidos;
		}

		public void setVendidos(int vendidos) {
			this.vendidos = vendidos;
		}

		// Metodos que va a heredar la clase MetodosArticulos.

		/**
		 * Metodo para vender un producto y agregarlo a una lista de ventas del dia.
		 * @param codigo Debe ser de tipo String.
		 */
		public void vender(String codigo) {

		}

		/**
		 * Metodo para imprimir las descripciones de los atributos.
		 * 
		 * @return Retorna un valor de tipo String.
		 */
		public String Impresion() {

			return null;
		}

		/**
		 * Metodo que registra cuantas ventas se hicieron al dia.
		 * 
		 * @return Retorna un valor de tipo String.
		 */
		public static String ventasDia() {
			return null;
		}

		/**
		 * Metodo para actualizar la cantidad de articulos de un producto dentro de la
		 * maquina expendedora.
		 * 
		 * @param codigo Debe ser de tipo String.
		 * @param update Debe ser de tipo Int.
		 * @return Retorna un valor de tipo Int.
		 */
		public int updateProducto(String codigo, int update) {
			// TODO Auto-generated method stub
			return 0;
		}

		/**
		 * Metodo para borrar un producto de la lista.
		 * 
		 * @param cod Debe ser de tipo String.
		 * @return Retorna un valor de tipo String.
		 */
		public static String deleteProducto(String cod) {
			// TODO Auto-generated method stub
			return null;
		}

		/**
		 * Metodo para registrar un nuevo producto.
		 * 
		 * @param codigo   Debe ser de tipo String.
		 * @param articulo Debe ser de tipo String.
		 * @param precio   Debe ser de tipo Double.
		 * @param cantidad Debe ser de tipo Int.
		 */
		public abstract void icreateProducto(String codigo, String articulo, double precio, int cantidad);

		/**
		 * Metodo para leer los datos del producto.
		 * 
		 * @param producto Objeto a leer.
		 */
		public void ireadProducto(Metodos producto) {
		}
		
		/*
		 * @deprecated
		 */
		public abstract String toString();

		public void createProducto1(String codigo, String articulo, double precio, int cantidad) {
			// TODO Auto-generated method stub
			
		}

	}

/**
 * Clase donde se presentan todos los metodos para ejecutar en Main Class.
 * 
 * @author dareyes@externosdeloittemx.com
 * @version 1.0
 * @since 2020-03-10
 * {@inheritDoc}
 */
public class Metodos extends Articulos {

	private static final Logger LOGGER = Logger.getLogger(Metodos.class.getName());;

	// Lista para almacenar productos vendidos.
	static ArrayList<Metodos> productosVendidos = new ArrayList<Metodos>();

	// Constructores heredados de la superClase Articulos.
	public Metodos(Metodos metodos) {
		super();

	}

	/**
	 * Constructor para heredar los atributos a cualquier objeto de la clase
	 * Articulos, MetodosArticulos.
	 * 
	 * @param codigo
	 * @param articulo
	 * @param precio
	 * @param cantidad
	 */
	public Metodos(String codigo, String articulo, double precio, int cantidad) {
		super(codigo, articulo, precio, cantidad);

	}

	// Metodos CRUD

	/**
	 * El metodo permite registrar un nuevo producto e imprime un texto de que fue
	 * correctamente registrado.
	 */
	public void icreateProducto(String codigo, String articulo, double precio, int cantidad) {
		new Metodos(codigo, articulo, precio, cantidad);
		LOGGER.info("PRODUCTO REGISTRADO CORRECTAMENTE.");

	}

	/**
	 * Esta clase busca el codigo del producto dentro de la lista principal y
	 * actualiza la cantidad.
	 * @exception Si el codigo no se encuentra dentro de la lista, muestra un error.
	 * @throws Error.
	 * @serial Recorre la lista de articulo en articulo de los datos registrados.
	 */
	public int updateProducto(String codigo, int update) {
		int actualizacion = 0;
		try {
			for (Articulos artic : Articulos.productos) {
				if (artic.getCodigo().equals(codigo)) {
					int aux = this.getCantidad();
					actualizacion = aux + update;
					LOGGER.info("PRODUCTO ACTUALIZADO CORRECTAMENTE");
				}
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Error. ", ex);
		}
		return actualizacion;

	}

	/**
	 * El metodo permite buscar un producto por Codigo y borrarlo los datos de
	 * lista. Imprime el valor del codigo que fue eliminado.
	 * 
	 * @param cod Valor de tipo String.
	 * @return Retorna un mensaje (String).
	 * @exception Si el codigo no se encuentra dentro de la lista, muestra un error.
	 * @throws Error.
	 */
	public static String deleteProducto(String cod) {
		String message = " ";
		try {
			for (Articulos artic : Articulos.productos) {

				if (artic.getCodigo().equals(cod)) {
					Articulos.productos.remove(artic);
					message = "El art�culo con c�digo: " + cod + " ha sido eliminado.";
					break;
				}
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Error. ", ex);
		}
		return message;

	}

	/**
	 * Imprimite un formato de acuerdo al Metodo Impresion.
	 */
	public void ireadProducto(Metodos producto) {
		LOGGER.info(this.Impresion());

	}

	/**
	 * El metodo imprime el formato para que aparezca los datos en consola.
	 * 
	 */
	@Override
	public String Impresion() {
		String status = "Codigo: " + this.getCodigo() + " Articulo: " + this.getArticulo() + " Precio: "
				+ this.getPrecio() + " Stock: " + this.getCantidad();

		return status;

	}

	/**
	 * El metodo permite buscar un producto por su codigo y agregarlo a la lista de
	 * vendidos. Si el producto no se encuentra dentro de la lista, aparece un texto
	 * de producto agotado.
	 * 
	 * @param codigo Debe ser de tipo String.
	 * @exception Si el codigo no se encuentra dentro de la lista, muestra un error.
	 * @throws Error.
	 */
	@Override
	public void vender(String codigo) {
		try {
			if (Articulos.productos.contains(this)) {
				Metodos aux = new Metodos(this);
				this.setCantidad(this.getCantidad() - 1);
				aux.setCantidad(1);
				productosVendidos.add(aux);
			} else {
				LOGGER.info("PRODUCTO AGOTADO.");
			}
		} catch (Exception e) {
			LOGGER.severe("Error " + e);

		}

	}

	/**
	 * Metodo que permite buscar un articulo dentro de una lista e imprimir cuantos
	 * articulos y cu�nto se vendi�.
	 * 
	 * @return Retorna un valor de tipo String.
	 * @exception Si el codigo no se encuentra dentro de la lista, muestra un error.
	 * @throws Error.
	 */
	public static String ventasDia() {
		String ventas = "";
		try {
			for (Articulos artic : productosVendidos) {
				ventas += artic.getArticulo() + "Productos vendidos: " + artic.getVendidos() + " Total: $"
						+ artic.getVendidos() * artic.getPrecio() + "\n";
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Error. ", ex);
		}

		return ventas;

	}

	@Override
	/**
	 * @deprecated Should no longer be used.
	 */
	/*
	public static void createProducto1(String codigo, String articulo, double precio, int cantidad) {
		// TODO Auto-generated method stub
		
	}

	@Override
	/**
	 * @deprecated Should no longer be used.
	 */
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

}
