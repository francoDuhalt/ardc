import java.util.Scanner;



public class Auto {

	int NumPuertas;
	  String Color;
	  String Marca;
	  double Precio;
	  static int ContadorAutos = 0;
	  
	  public Auto()
	  {
	      NumPuertas = 0; //fd
	      Color = "";
	      Marca = "";
	      Precio = 0;
	  }
	  
	 
	  
	  public Auto(int numPuertas, String color, String marca, double precio) {
		super();
		NumPuertas = numPuertas;
		Color = color;
		Marca = marca;
		Precio = precio;
	}

	//GETS Y SETS
	  public String getMarca()
	  {
	      return Marca;
	  }
	  public void setMarca(String marca)
	  {
	    Marca = marca;    
	  }
	  public double getPrecio()
	  {
	      return Precio;
	  }
	  public void setPrecio(double precio)
	  {
	    Precio = precio;    
	  }
	  public int getNumPuertas()
	  {
	      return NumPuertas;
	  }
	  public void setNumPuertas(int numpuertas)
	  {
	   NumPuertas = numpuertas;    
	  }
	  public String getColor()
	  {
	      return Color;
	  }
	  public void setColor(String color)
	  {
	    Color = color;    
	  } 
	  public static  int getContador()
	  {
	      return ContadorAutos;
	  }
	  public static void setContador(int contador )
	  {
	      ContadorAutos = contador;
	  }
	   @Override
	   public String toString()
	   {
	       return String.format("Datos del Auto\n"
	               + "Marca: %s\nPrecio: %.2f\nNum. Puertas: %d\n"
	               + "Color: %s\nContador: %d", getMarca(), getPrecio(), 
	               getNumPuertas(), getColor(), getContador() );
	   }
	   
	   /**
	    * Metodo que se encarga de recibir los datos ingresados por el usuarios
	    * 
	    * @author nnavarrete@externosdeloittemx.com
	    * @version 1.0
	    * @since 08/03/2020
	    */
	   public void EntradaDatos ()
	   {
	       String marca;
	       String color;
	       int numpuertas;
	       double precio;
	       Scanner datos = new Scanner(System.in);
	       System.out.printf("Entra la marca del auto: ");
	       marca = datos.nextLine();
	       System.out.printf("Entra el color del auto: ");
	       color = datos.nextLine();
	       System.out.printf("Entra el nùmero de puertas: ");
	       numpuertas = datos.nextInt();
	       System.out.printf("Entra el precio del auto ");
	       precio = datos.nextDouble();
	       
	       setMarca(marca);
	       setColor(color);
	       setNumPuertas(numpuertas);
	       setPrecio(precio);
	   }
	   
	   /**
	    * Metodo que se encarga de agregar al arraylist 'listaAutos', los datos ingresados
	    * por el usuario
	    * 
	    * @author nnavarrete@externosdeloittemx.com
	    * @version 1.0
	    * @since 08/03/2020
	    * @param listaAutos ArrayList
	    */
	   public static void Alta(Auto listaAutos[])
	   {
	     int posicion = -1;
	     boolean encontrado = false;
	     if(Auto.ContadorAutos >= 5)
	     {
	         System.out.println("La lista esta llena");
	     }
	     else
	     {
	         for(int i = 0; i < listaAutos.length; i++)
	         {
	          if(listaAutos[i].getMarca().equalsIgnoreCase(""))
	          {
	           posicion = i;
	           encontrado = true;
	           break;
	          }
	         }
	         if(encontrado == false)
	         {
	             System.out.println("No se encontro un lugar vacio en el arreglo.");
	         }
	         else 
	         {
	             listaAutos[posicion].EntradaDatos();
	             Auto.ContadorAutos++;
	             System.out.println("Se inserto con exito el auto en la lista");
	         }//else si hubo lugar 
	     }       //else si esta llena la lista     
	   }         //Alta
	  
	   /**
	    * Metodo que se encarga de agregar los datos recibidos en el metodo de EntradaDatos
	    * 
	    * 
	    * @param listaAutos ArrayList
	    * @param marca string 
	    * @param precio double
	    * @param numpuertas int
	    * 
	    * @author nnavarrete@externosdeloittemx.com
	    * @version 1.0
	    * @since 08/03/2020
	    */
	   public static void agregar(Auto listaAutos[], String marca, double precio, int numpuertas){
	       int posicion = -1;
	     boolean encontrado = false;
	     if(Auto.ContadorAutos >= 5)
	     {
	         System.out.println("La lista esta llena");
	     }
	     else
	     {
	         for(int i = 0; i < listaAutos.length; i++)
	         {
	          if(listaAutos[i].getMarca().equalsIgnoreCase(""))
	          {
	           posicion = i;
	           encontrado = true;
	           break;
	          }
	         }
	         if(encontrado == false)
	         {
	             System.out.println("No se encontro un lugar vacio en el arreglo.");
	         }
	         else 
	         {
	             listaAutos[posicion].EntradaDatos();
	             Auto.ContadorAutos++;
	             System.out.println("Se inserto con exito el auto en la lista");
	         }
	     }
	  //    return "realizado";
	   }
	   
	   /**
	    * metodo que muestra los registros actuales en la lista
	    * @param listaAuto arrayList
	    * 
	    * 
	    * @author nnavarrete@externosdeloittemx.com
	    * @version 1.0
	    * @since 08/03/2020
	    */
	   public static void ConsultaGeneral(Auto listaAuto[])
	   {
	     for(int i = 0; i < listaAuto.length; i++) 
	     {
	         System.out.println(listaAuto[i]);
	     }
	   }
	   
	   /**
	    *Metodo que se encarga de consultar un registro por medio del color del auto. 
	    * @param listaAuto ArrayList
	    *
	    *
	    *@author nnavarrete@externosdeloittemx.com
	    *@version 1.0
	    *@since 08/03/2020
	    */
	   public static void ConsultaNombre (Auto listaAuto[])
	   {
	    String color;
	    boolean encontrado = false;
	    Scanner datos = new Scanner(System.in);
	    System.out.printf("Entra el color del auto: ");
	    color = datos.nextLine();   
	    
	    for(int i = 0; i < listaAuto.length; i++)
	    {
	     if(listaAuto[i].getColor().equalsIgnoreCase(color))   
	     {
	      //listaAuto[i].EntradaDatos();
	         System.out.println(listaAuto[i]);
	         encontrado = true;
	     }    
	    }
	    if(encontrado == false)
	    {
	        System.out.println("No se encontrò el auto");   
	    }    
	    else
	    
	 System.out.println("Dato encontrado");

	   }
	   
	   /**
	    * Metodo que se encarga de realizar cambios al registro almacenado en la lista,
	    * todo esto por medio de la marca a ingresar por el usuario.
	    * @param listaAuto
	    * 
	    * 
	    * @author nnavarrete@externosdeloittemx.com
	    * @version 1.0
	    * @since 08/03/2020
	    */
	   public static void ModificarAuto(Auto listaAuto[])
	   {
	    String marca;
	    boolean encontrado = false;
	    Scanner datos = new Scanner(System.in);
	    System.out.printf("Entra la marca del auto: ");
	    marca = datos.nextLine();   
	    
	    for(int i = 0; i < listaAuto.length; i++)
	    {
	     if(listaAuto[i].getMarca().equalsIgnoreCase(marca))   
	     {
	      listaAuto[i].EntradaDatos();
	      encontrado = true;
	     }    
	    }
	    if(encontrado == false)
	    {
	        System.out.println("No se encontrò el auto");   
	    }    
	    else
	     System.out.println("Se actualizaon los datos correctamente");   
	   }        
	   
	   /**
	    * Metodo que se encarga de eliminar el registro, por medio del atributo 'marca'.
	    * 
	    * @param listaAuto ArrayList
	    * 
	    * 
	    * @author nnavarrete@externosdeloittemx.com
	    * @version 1.0
	    * @since 08/03/2020
	    */
	  public static void EliminarAuto(Auto listaAuto[])
	   {
	    String marca;
	    boolean encontrado = false;
	    Scanner datos = new Scanner(System.in);
	    System.out.printf("Entra la marca del auto: ");
	    marca = datos.nextLine();   
	    
	    for(int i = 0; i < listaAuto.length; i++)
	    {
	     if(listaAuto[i].getMarca().equalsIgnoreCase(marca))   
	     {
	      listaAuto[i].BorrarDatos();
	      encontrado = true;
	     }    
	    }
	    if(encontrado == false)
	    {
	        System.out.println("No se encontrò el auto");   
	    }    
	    else
	     System.out.println("Se actualizaon los datos correctamente");   
	   }   
	  
	  /**
	   * metodo que borra los datos 
	   * 
	   * @author nnavarrete@externosdeloittemx.com
	   * @version 1.0
	   * @since 08/03/2020 
	   */
	  public void BorrarDatos()
	  {
	   
	      NumPuertas = 0;
	      Color = "";
	      Marca = "";
	      Precio = 0;
	  
	  
	  }
}
